package tys.c6

import tys.c6

final object C3_ChildOfChild extends Child("hoge", 1) {
    override def doSomething(value: String) = println("hoge")
}
