package tys.c6

class Child(val aname: String, val avalue: Int) extends Parent {

    override def doSomething(value: String) = {
        println(value)
    }

}