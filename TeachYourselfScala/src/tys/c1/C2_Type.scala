package tys.c1

object C2_Type {

    def main(args: Array[String]) {
        var byteVal: Byte = 1
        var shortVal: Short = 1
        var longVal: Long = 1
        var intVal: Int = 1
        var charVal: Char = '1'
        var stringVal: String = "1"
        var floadVal: Float = 1.0F
        var doubleVal: Double = 1.0e1D
        var booleanVal: Boolean = false

        println(String.join(" ", intVal.toString(), doubleVal.toString()))

        println("\n\n")
        println("""\\\///&&&((()))""")

        val s = 'sSymbol
        println(s.name)
    }

}