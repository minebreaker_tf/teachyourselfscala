package tys.c1

object C1_Variables {

    def main(args: Array[String]) {
        var num: Int = 10
        println(num)

        num = 20
        println(num)

        var n1, n2, n3 = 1
        println(n1 + n2 + n3)

        val message = "hello"
        // message = "goodbye"

    }

}