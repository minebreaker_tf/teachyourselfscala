package tys.c1

object C3_Arrays {

    def main(args: Array[String]) {
        val array1 = new Array[Int](10)
        val array2 = Array(1, 2, 3)

        array2(0) = 100
        println(array2(0))
        println(array2.length)
    }

}