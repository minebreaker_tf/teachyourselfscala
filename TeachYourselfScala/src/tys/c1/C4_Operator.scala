package tys.c1

object C4_Operator {

    def main(args: Array[String]) {
        println(1 + 2)
        println(1.+(2)) // As function

        val p = 1
        val q = 2
        println(p == 2)
        println(p != q)

        val s1 = "hoge"
        val s2 = "piyo"
        println(s1 == s2)
    }

}