package tys.c3

object C4_Match {

    def main(args: Array[String]) {

        val x = 0

        val result = x match {
            case 10 => {
                println(x)
            }
            case 20 => {
                println(x)
            }
            case _ => {
                println(x)
                "default"
            }
        }
        println("result: " + result)

        val result2 = x match {
            case a if a == 0 => println(a)
            case _ => println("no match found")
        }
        println("result2: " + result2)

    }

}