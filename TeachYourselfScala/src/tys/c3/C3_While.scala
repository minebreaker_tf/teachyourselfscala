package tys.c3

object C3_While {

    def main(args: Array[String]) {

        var i = 0
        while (i < 10) {
            println("i = " + i)
            // i++ // can't use increment operator
            i += 1
        }

        do {
            println("i = " + i)
            i += 1
        } while (i < 10)

    }

}