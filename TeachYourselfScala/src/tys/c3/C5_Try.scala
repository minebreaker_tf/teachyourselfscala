package tys.c3

object C4_Try {

    def main(args: Array[String]) {

        val result = try {
            throw new Exception("thrown!")
        } catch {
            case e: Exception => {
                println(e)
                -1
            }
        } finally {
            println("closing")
        }

        println("result is " + result)

    }

}