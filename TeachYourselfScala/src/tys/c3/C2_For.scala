package tys.c3

object C2_For {

    def main(args: Array[String]) {

        val list = List("hoge", "piyo", "hogera")

        for (item <- list) {
            println("itme: " + item)
        }

        for (
                item <- list         // generator
                if item.length() > 4 // filter
        ) {
            println(item)
        }

        val result = for (
                item <- list
                if item.length() <= 4) yield {
            println(item)
            "items in new list: " + item
        }
        println(result)

    }

}