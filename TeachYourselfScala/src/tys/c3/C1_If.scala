package tys.c3

object C1_If {

    def main(args: Array[String]) {

        val x1 = 0
        if (x1 == 0) {
            println("x1 = 0")
        } else {
            println("x1 != 0")
        }

        val x2 = 1
        val y2 = if (x2 == 0) {
            "x is zero"
        } else {
            "x is not zero"
        }
        println(y2)

        val y3 = if (false) "if else..."
        println(y3)

    }

}