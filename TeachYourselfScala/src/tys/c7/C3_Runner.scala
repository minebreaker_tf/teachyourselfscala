package tys.c7

object C3_Runner {

    def main(args: Array[String]) {

        val parent = Child
        parent.introduce

        val other = new Mixin with Parent
        other.introduce

        val another = new Parent{}
        another.introduce

    }
}