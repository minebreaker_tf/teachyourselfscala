package tys.c2

object C3_Tuple {

    def main(args: Array[String]) {

        val tuple1: Tuple2[String, Int] = Tuple2("hoge", 1)
        val tuple2 = ("piyo", 2)

        println(tuple1)
        println(tuple2._1)
        println(tuple2._2)

    }

}