package tys.c2

object C1_Seq {

    def main(args: Array[String]) {

        val list1: List[Int] = List(1)
        println(list1)
        val list2 = 2 :: list1
        println(list2)
        println(list2(1))

        // list1(0) = "123" // Immutable

        val emptyList = Nil
        val list3 = "NewItem" :: list2
        println(list3)
        val list4 = list3.::("AnotherItem")

        println(list4)
        println("head:    " + list4.head.toString())
        println("tail:    " + list4.tail.toString())
        println("isEmpty: " + list4.isEmpty)
        println("length:  " + list4.length)

    }

}