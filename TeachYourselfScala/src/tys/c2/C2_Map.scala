package tys.c2

object C2_Map {

    def main(args: Array[String]) {

        val map1: Map[String, String] = Map("a" -> "hoge", "b" -> "piyo")
        println(map1)
        println(map1("a"))
        // println(map1("c")) // throws exception
    }

}