package tys.c5

object C4_Case {

    def main(args: Array[String]) {

        val apple = CaseApple("fuji")
        println(apple.name)
        println(apple.toString())

        def matchTest(value: Any) = {
            value match {
                case CaseApple(name) => println(name)
                case _ => println("ohter")
            }
        }

        matchTest(apple)

        val other = apple.copy(name = "jonaglod")
        println(other)



    }

}
