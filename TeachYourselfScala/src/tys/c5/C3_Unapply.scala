package tys.c5

object C3_Unapply {

    def main(args: Array[String]) {

        def matchTest(value: Any) = {
            value match {
                case Apple => println("Apple")
                case Orange("DEKOPON") => println("Orange.name = DEKOPON")
                case _ => println("other")
            }
        }

        matchTest(Orange("something"))
        matchTest(Orange("DEKOPON"))
        matchTest(Apple)

    }

}
