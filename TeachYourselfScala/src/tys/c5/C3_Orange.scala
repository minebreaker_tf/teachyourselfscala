package tys.c5

class Orange(val name: String)
object Orange {
    def apply(name: String): Orange = new Orange(name)
    def unapply(a: Orange): Option[String] = Some(a.name)
}
