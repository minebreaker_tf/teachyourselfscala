package tys.c5

object C2_Option {

    def main(args: Array[String]) {

        val map = Map(1 -> "one", 2 -> "two")
        println("1: " + map.get(1))
        println("3: " + map.get(3))
        println("3: " + map.getOrElse(3, "default"))

        def testMatch(option: Option[Int]) = {
            option match {
                case Some(n) => println(n)
                case None => println("None")
            }
        }

        testMatch(Some(10))
        testMatch(None)

    }

}