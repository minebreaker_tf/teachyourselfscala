package tys.c5

class Apple
object Apple {
    def unapply(a: Any): Boolean = {
        if (a.isInstanceOf[Apple]) true else false
    }
}
