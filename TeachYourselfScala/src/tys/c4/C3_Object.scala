package tys.c4

object C3_Object {

    private val scala = "you don't have static method in scala, but can use 'object' to create singleton class."

    def main(args: Array[String]) {
        introduce()
    }

    def introduce() = println(scala)

}
