package tys.c4

class C4_CompanionSample private(value: String) {
    private val introduction =
        "this is companion object, whitch can access to the private field or method of the class that has same name."

    def anotherFunction = {
        println(value)
    }
}

object C4_CompanionSample {
    def introduce = {
        val sampleClass = new C4_CompanionSample("") // private primary constructor
        println(sampleClass.introduction)            // able to access private field
    }

    def apply(value: String) = {
        new C4_CompanionSample(value)
    }
}

// must be declared in same package
object C4_CompanionObject {
    def main(args: Array[String]) {

        C4_CompanionSample.introduce

        val created = C4_CompanionSample("you can use this as a factory method") // 'apply' can be hidden
        created.anotherFunction
    }
}

