package tys.c4

class C2_Class(var arg: String) {

    var field = "field"
    private var privateField = "you cant see me"

    println("primary constructor call")

    def this() = this("auxiliary constructor")

    require(arg != null) // throws exception if 'arg' was null

    def callMe() = println("function is called; constructor arg was " + arg)

}
