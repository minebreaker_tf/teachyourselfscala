package tys.c4

object C2_Class_Run {
    def main(args: Array[String]) {

        val newClass: C2_Class = new C2_Class("ARG")
        newClass.callMe()

        val anotherClass = new C2_Class
        anotherClass.callMe()

    }
}
