package tys.c4

object C1_Function {

    def main(args: Array[String]) {

        println(add(1, 1))
        hello

        val function = (x: Int, y: Int) => x + y
        val func2: (Int, Int) => Int = (x: Int, y: Int) => x + y

        println(function(2, 2))
        println(func2(3, 3))

        val funcGot = getFunc("gotten: ")
        println(funcGot(4, 4))

        varArg("hoge", "piyo")
        varArg("foo", "bar", "buz")

        message()
        message("good bye!")

        val placeHolderUsage: (Int, Int) => Int = {
            _ + _
        }

        println(placeHolderUsage(5, 5))

        val full = add _
        val partial = add(6, _: Int)
        println(partial(6))

        count() // do nothing

        val closure = count()
        // closure // pure function expression: do nothing
        closure()
        closure()
        val closure2 = count()
        closure2()

        def say(msg: String, name: String) = {
            println(name + " said: " + msg)
        }

        say("hello", "john")
        say("good night", "jack")

    }

    def add(x: Int, y: Int): Int = {
        x + y
    }

    def hello: Unit = {
        println("Hello!")
    }

    def getFunc(str: String): (Int, Int) => String = {
        (x: Int, y: Int) =>
            {
                str + (x + y)
            }
    }

    def varArg(args: String*) = {
        for (arg <- args) {
            println(arg)
        }
    }

    def message(message: String = "hello!") = {
        println(message)
    }

    def count() = {
        var count = 0
        () => {
            count += 1
            println(count)
        }
    }

}